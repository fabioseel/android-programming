# android-programming
# Entwicklungsziel
Dieses Repository dient zur Entwicklung der WG-App im Rahmen der Android-Programmierung Vorlesung.
Die App soll die folgenden Funktionen bieten:
* Anschaffungs-Abstimmungs-Liste (aka specialized Doodle)
* To Do List mit Zuweisungsfunktion (Putzplan etc.)
* Parkplaner

# Mitarbeit
Klone das Repo, leg los!
* git clone https://gitlab.com/fabioseel/android-programming.git
Genauer Commit/Branching Workflow muss noch festgelegt werden.

# Mitarbeiter
* Jonas Nahstoll
* Fabio Seel
* Adrian Stock

# Matrikelnummern
4808954, 9722150, 6529562
