package com.studios.bobbycarpirat.wgapplication.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.studios.bobbycarpirat.wgapplication.model.Todo;

import java.util.ArrayList;
import java.util.List;


//tutorial: https://guides.codepath.com/android/local-databases-with-sqliteopenhelper#using-our-database-handler
public class TodoDatabaseHelper extends SQLiteOpenHelper {
    private static TodoDatabaseHelper sInstance;

    //TODO: Method never used -> Correct? or wrong use of singleton pattern
    public static synchronized TodoDatabaseHelper getInstance(Context context) {
        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new TodoDatabaseHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    // Database Info
    private static final String DATABASE_NAME = "todoDatabase";
    private static final int DATABASE_VERSION = 1;

    // Table Names
    private static final String TABLE_TODOS = "todos";

    // Post Table Columns
    private static final String KEY_TODO_ID = "id";
    private static final String KEY_ITEM_TEXT = "text";
    private static final String KEY_ITEM_STATUS = "status";
    //private static final String KEY_CUSTOM_ID = "customid";


    public TodoDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    // Called when the database connection is being configured.
    // Configure database settings for things like foreign key support, write-ahead logging, etc.
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    // Called when the database is created for the FIRST time.
    // If a database already exists on disk with the same DATABASE_NAME, this method will NOT be called.
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TODOS_TABLE = "CREATE TABLE " + TABLE_TODOS +
                "(" +
                KEY_TODO_ID + " INTEGER PRIMARY KEY," + // Define a primary key
                //KEY_CUSTOM_ID + "STRING" +
                KEY_ITEM_TEXT + " STRING," +
                KEY_ITEM_STATUS+ " BOOLEAN" +
                ")";


        db.execSQL(CREATE_TODOS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    // Insert a post into the database
    public void addTodo(Todo todo) {
        // Create and/or open the database for writing
        SQLiteDatabase db = getWritableDatabase();

        // It's a good idea to wrap our insert in a transaction. This helps with performance and ensures
        // consistency of the database.
        db.beginTransaction();
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_ITEM_TEXT, todo.getName());
            if (todo.getDone())
            values.put(KEY_ITEM_STATUS, "true");
            else
                values.put(KEY_ITEM_STATUS, "false");

            // Notice how we haven't specified the primary key. SQLite auto increments the primary key column.
            db.insertOrThrow(TABLE_TODOS, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.d(e.toString(), "Error while trying to add post to database");
        } finally {
            db.endTransaction();
        }
    }

    public List<Todo> getTodos() {
        List<Todo> todos = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        String[] field = {KEY_TODO_ID,KEY_ITEM_TEXT,KEY_ITEM_STATUS};
        Cursor c = db.query(TABLE_TODOS, field, null, null, null, null, null);

        int itext = c.getColumnIndex(KEY_ITEM_TEXT);
        int istatus = c.getColumnIndex(KEY_ITEM_STATUS);
        int iid = c.getColumnIndex(KEY_TODO_ID);

        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){
            String text = c.getString(itext);
            String status = c.getString(istatus);
            Integer rowid = c.getInt(iid);
            todos.add(new Todo(text,status,String.valueOf(rowid)));
        }
        return todos;
    }


    // Update
    public int updateTodo(Todo todo, int index) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_ITEM_STATUS, todo.getDoneString());
        values.put(KEY_ITEM_TEXT, todo.getName());

// TODO: 03.12.2018 make secure
        //"id = '?'", 
        return db.update(TABLE_TODOS, values, "id = '" +index+"'",null);
    }

    public int removeTodo(int index) {
        SQLiteDatabase db = this.getWritableDatabase();
// TODO: 03.12.2018 make secure
        return db.delete(TABLE_TODOS,  "id = '" + index+"'", null);
    }
}
