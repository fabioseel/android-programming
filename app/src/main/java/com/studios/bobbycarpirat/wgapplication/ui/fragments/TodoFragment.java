package com.studios.bobbycarpirat.wgapplication.ui.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.studios.bobbycarpirat.wgapplication.R;
import com.studios.bobbycarpirat.wgapplication.database.TodoDatabaseHelper;
import com.studios.bobbycarpirat.wgapplication.model.Todo;
import com.studios.bobbycarpirat.wgapplication.ui.adapter.TodoRecyclerAdapter;
import com.studios.bobbycarpirat.wgapplication.ui.listener.HideButtonOnScrollListener;
import com.studios.bobbycarpirat.wgapplication.ui.listener.RecyclerItemTouchHelper;

public class TodoFragment extends Fragment {

    private RecyclerView todoRecyclerView;
    String newItem = "";
    TodoRecyclerAdapter todoAdapter;
    TodoDatabaseHelper tdbh;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_todo, container, false);
        todoRecyclerView = view.findViewById(R.id.todo_recycler_view);

        tdbh = new TodoDatabaseHelper(getContext());

        FloatingActionButton newVotingButton = view.findViewById(R.id.new_todo_button);
        newVotingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newItem = "";
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(getString(R.string.title_new_todo_alert));

                // Set up the input
                final EditText input = new EditText(getActivity());
                // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton(getString(R.string.accept_button_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        newItem = input.getText().toString();
                        addItem(newItem);
                    }
                });
                builder.setNegativeButton(getString(R.string.cancel_button_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                // TODO: make Keyboard show up automatically on start of the dialog (as well in Login Activity and VotingFragment neede)
                builder.show();
            }
        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        todoRecyclerView.addOnScrollListener(new HideButtonOnScrollListener(newVotingButton));
        todoRecyclerView.setLayoutManager(mLayoutManager);
        todoRecyclerView.setItemAnimator(new DefaultItemAnimator());
        todoRecyclerView.addItemDecoration(new DividerItemDecoration(this.getContext(), LinearLayoutManager.VERTICAL));

        todoAdapter = new TodoRecyclerAdapter(this);
        todoRecyclerView.setAdapter(todoAdapter);

        // Hinzufügen des "per Swipe-Lösch"-Adapters
        ItemTouchHelper.Callback callback = new RecyclerItemTouchHelper(todoAdapter);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(todoRecyclerView);

        return view;
    }

    public void addItem(String newItem) {
        if (!newItem.isEmpty()) {
            tdbh.addTodo(new Todo(newItem,false));
            todoAdapter.update();
        }
    }
}
