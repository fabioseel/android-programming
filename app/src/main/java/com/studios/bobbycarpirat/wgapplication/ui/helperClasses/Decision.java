package com.studios.bobbycarpirat.wgapplication.ui.helperClasses;

public enum Decision {
    GREEN,
    ORANGE,
    RED
}
