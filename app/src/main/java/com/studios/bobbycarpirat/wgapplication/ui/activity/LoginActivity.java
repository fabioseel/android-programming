package com.studios.bobbycarpirat.wgapplication.ui.activity;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.studios.bobbycarpirat.wgapplication.R;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.SharedPreferencesKeys;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private Button loginButton;


    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);

        TextView greetingView = findViewById(R.id.greetingView);
        if (sharedPref.contains(SharedPreferencesKeys.USERNAME)){
            greetingView.setText(getString(R.string.hello_text)+sharedPref.getString(SharedPreferencesKeys.USERNAME,"")+"!");
        }


        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        loginButton = findViewById(R.id.button_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                EditText editUsername = findViewById(R.id.editTextUsername);
                String userName = editUsername.getText().toString();



                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(SharedPreferencesKeys.USERNAME,userName);
                editor.commit();

                TextView firstTextView = findViewById(R.id.greetingView);
                userName = getString(R.string.hello_text)+" "+sharedPref.getString(SharedPreferencesKeys.USERNAME,"")+"! :)";
                firstTextView.setText(userName);
            }
        });


    }


    @Override
    public void onStart() {
        super.onStart();
        // TODO: Check if user is signed in (non-null) and update UI accordingly.
        // FirebaseUser currentUser = mAuth.getCurrentUser();
        //updateUI(currentUser);
    }


}
