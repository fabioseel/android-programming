package com.studios.bobbycarpirat.wgapplication.ui.helperClasses;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.UUID;

public class IdManager {

    private static DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    static DatabaseReference mTestRef = mRootRef.child("IDs");
    static ArrayList<String> keys = new ArrayList<>();

    static ValueEventListener testEntryListener = new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            if (keys.size() == 0) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    keys.add(snapshot.getKey());
                }
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
        }
    };

    public static void init() {
        mTestRef.addListenerForSingleValueEvent(testEntryListener);
    }

    public static String getUniqueId() {
        String newUUID = UUID.randomUUID().toString();
        while (keys.contains(newUUID)) {
            newUUID = UUID.randomUUID().toString();
        }
        DatabaseReference newIdEntry = mTestRef.child(newUUID);
        newIdEntry.setValue("");
        keys.add(newUUID);
        return newUUID;
    }

    public static String getUniqueIdWithoutChecking() {
        String newUUID = UUID.randomUUID().toString();
        return newUUID;
    }

    public static void removeId() {}
}
