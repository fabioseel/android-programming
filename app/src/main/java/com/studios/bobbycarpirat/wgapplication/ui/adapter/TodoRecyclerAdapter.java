package com.studios.bobbycarpirat.wgapplication.ui.adapter;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.studios.bobbycarpirat.wgapplication.R;
import com.studios.bobbycarpirat.wgapplication.database.TodoDatabaseHelper;
import com.studios.bobbycarpirat.wgapplication.model.Todo;
import com.studios.bobbycarpirat.wgapplication.ui.fragments.TodoFragment;
import com.studios.bobbycarpirat.wgapplication.ui.listener.RecyclerItemTouchHelper;

import java.util.List;


public class TodoRecyclerAdapter extends RecyclerView.Adapter<TodoRecyclerAdapter.PersVotingViewHolder> implements RecyclerItemTouchHelper.ItemTouchHelperAdapter {
    private final TodoDatabaseHelper tdbh;
    private List<Todo> todoList;
    TodoFragment localFragment;

    public TodoRecyclerAdapter(TodoFragment fragment) {
        localFragment = fragment;
        tdbh = new TodoDatabaseHelper(fragment.getContext());
        //get TodoList from Database
        this.todoList = tdbh.getTodos();

        if (todoList.isEmpty())
            exampleTodos();
        }

    private void exampleTodos() {
        tdbh.addTodo(new Todo(localFragment.getContext().getString(R.string.explanation_add_todo),false));
        tdbh.addTodo(new Todo(localFragment.getContext().getString(R.string.explanation_change_todo_state),true) );
        tdbh.addTodo(new Todo(localFragment.getContext().getString(R.string.explanation_delete_todo),false));
        update();
    }

    @NonNull
    @Override
    public PersVotingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.todo_list_item, viewGroup, false);

        return new PersVotingViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PersVotingViewHolder persVotingViewHolder, int position) {
        String persVoting = todoList.get(position).getName();
        Boolean persStat = todoList.get(position).getDone();
        persVotingViewHolder.todoItem.setText(persVoting);
        //green if done, red if not done
        if (persStat) {
            persVotingViewHolder.setPersVotingRes(1);
        }
        else {
            persVotingViewHolder.setPersVotingRes(0);
        }
    }

    @Override
    public int getItemCount() {
        return todoList.size();
    }

    @Override
    public void removeItem(int position) {
        int positionInDb = todoList.get(position).getCustomid();
        tdbh.removeTodo(positionInDb);
        update();
    }

    public class PersVotingViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        final TextView todoItem;
        final ImageView todoCheckbox;

        PersVotingViewHolder(@NonNull View itemView) {
            super(itemView);
            todoItem = itemView.findViewById(R.id.todo_item);
            todoCheckbox = itemView.findViewById(R.id.todo_checkbox);

            ImageView colorView = itemView.findViewById(R.id.todo_checkbox);
            colorView.setOnClickListener(this);
        }

        private void setPersVotingRes(int i)
        {
            switch (i) {
                case 0: {
                    todoCheckbox.setColorFilter(itemView.getContext().getResources().getColor(R.color.trLightRed));
                    break;
                }
                case 1: {
                    todoCheckbox.setColorFilter(itemView.getContext().getResources().getColor(R.color.trLightGreen));
                    break;
                }
                default: {
                    todoCheckbox.setColorFilter(itemView.getContext().getResources().getColor(R.color.browser_actions_bg_grey));
                    break;
                }
            }
        }

        @Override
        public void onClick(View v) {

            int clickpos = getAdapterPosition();
            int i = todoList.get(clickpos).getCustomid();
            //alternate to-do done status in database
            if (todoList.get(clickpos).getDone()){

                tdbh.updateTodo(new Todo(todoList.get(clickpos).getName(),false),i);
                //force update
                update();
            }
            else{
                tdbh.updateTodo(new Todo(todoList.get(clickpos).getName(),true),i);
                //force update
                update();
            }
        }
    }

    public void update() {
        todoList = tdbh.getTodos();
        notifyDataSetChanged();
    }
}
