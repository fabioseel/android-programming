package com.studios.bobbycarpirat.wgapplication.model;

public class WG {

    private String name;
    private String ort;
    private String id;

    private WG(String name, String ort) {
        this.name = name;
        this.ort = ort;
    }

    public WG(String name) {
        this(name, "");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOrt() {
        return ort;
    }

    public void setOrt(String ort) {
        this.ort = ort;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }
}
