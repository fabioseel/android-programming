package com.studios.bobbycarpirat.wgapplication.ui.adapter;

import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.studios.bobbycarpirat.wgapplication.R;
import com.studios.bobbycarpirat.wgapplication.model.WG;
import com.studios.bobbycarpirat.wgapplication.ui.activity.MainActivity;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.SharedPreferencesKeys;
import com.studios.bobbycarpirat.wgapplication.ui.listener.RecyclerItemTouchHelper;

import java.util.List;

public class WGRecyclerAdapter extends RecyclerView.Adapter<WGRecyclerAdapter.WGViewHolder> implements RecyclerItemTouchHelper.ItemTouchHelperAdapter {

    private final List<WG> wgList;

    public WGRecyclerAdapter(List<WG> wgList)
    {
        this.wgList=wgList;
    }

    private SharedPreferences preferences;

    @NonNull
    @Override
    public WGViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.wg_list_item, viewGroup, false);

        WGViewHolder wgViewHolder = new WGViewHolder(itemView);
        wgViewHolder.setPreferences(preferences);

        return wgViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull WGViewHolder wgViewHolder, int position) {
        WG wg = wgList.get(position);
        wgViewHolder.wgNameTextView.setText(wg.getName());
        wgViewHolder.wgOrtTextView.setText(wg.getOrt());
        wgViewHolder.setId(wg.getId());
    }

    @Override
    public int getItemCount() {
        return wgList.size();
    }


    public void setPreferences(SharedPreferences newPreferences) {
        preferences = newPreferences;
    }

    @Override
    public void removeItem(int position) {
        // TODO: Löschen/Austreten von WGs
        notifyDataSetChanged();
    }

    public class WGViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        final ConstraintLayout listItem;
        final TextView wgNameTextView;
        final TextView wgOrtTextView;
        SharedPreferences preferences;
        String wg_id;


        WGViewHolder(@NonNull View itemView) {
            super(itemView);
            listItem = itemView.findViewById(R.id.ll_wg_list_item);
            wgNameTextView = itemView.findViewById(R.id.wg_name);
            wgOrtTextView = itemView.findViewById(R.id.wg_ort);


            listItem.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(SharedPreferencesKeys.WG_NAME, wgNameTextView.getText().toString());
            editor.putString(SharedPreferencesKeys.WG_ORT, wgOrtTextView.getText().toString());
            editor.putString(SharedPreferencesKeys.WG_ID, wg_id);
            editor.commit();
            // TODO: 03.12.2018 Test this
            MainActivity mainActivity = (MainActivity) v.getContext();
            mainActivity.showVotingFragment();

            Snackbar.make(mainActivity.getCurrentFocus(), wgNameTextView.getText().toString() + " " + v.getContext().getString(R.string.was_chosen), Snackbar.LENGTH_LONG).show();
        }

        public void setPreferences(SharedPreferences newPreferences) {
            preferences = newPreferences;
        }

        public void setId(String new_wg_id) {
            wg_id = new_wg_id;
        }
    }

}
