package com.studios.bobbycarpirat.wgapplication.ui.helperClasses;

public final class SharedPreferencesKeys {
    public static final String USERNAME="User_name";
    public static final String USER_INFO="User_info";
    public static final String USER_ID="User_id";

    public static final String WG_INFO="WG_info";
    public static final String WG_ID="WG_id";
    public static final String WG_NAME="WG_name";
    public static final String WG_ORT="WG_ort";
}
