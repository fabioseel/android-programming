package com.studios.bobbycarpirat.wgapplication.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.studios.bobbycarpirat.wgapplication.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PersVotingRecyclerAdapter extends RecyclerView.Adapter<PersVotingRecyclerAdapter.PersVotingViewHolder> {

    private final List<String> persVotingList;
    private final List<String> persVotingListResults;

    public PersVotingRecyclerAdapter(HashMap<String, String> newPersVotingList) {
        persVotingList = new ArrayList<>();
        for (Object key : newPersVotingList.keySet().toArray()) {
            persVotingList.add(key.toString());
        }
        persVotingListResults = new ArrayList<>();
        for (Object value : newPersVotingList.values().toArray()) {
            persVotingListResults.add(value.toString());
        }
    }

    @NonNull
    @Override
    public PersVotingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.pers_voting_list_item, viewGroup, false);

        return new PersVotingViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PersVotingViewHolder persVotingViewHolder, int position) {
        String persVoting = persVotingList.get(position);
        persVotingViewHolder.persVotingName.setText(persVoting);
        persVotingViewHolder.setPersVotingRes(persVotingListResults.get(position));
    }

    @Override
    public int getItemCount() {
        return persVotingList.size();
    }


    public class PersVotingViewHolder extends RecyclerView.ViewHolder {
        final TextView persVotingName;
        final ImageView persVotingRes;

        PersVotingViewHolder(@NonNull View itemView) {
            super(itemView);
            persVotingName = itemView.findViewById(R.id.voting_pers_name);
            persVotingRes = itemView.findViewById(R.id.pers_tr_light);
        }

        public void setPersVotingRes(String result)
        {
            switch (result) {
                case "Ja": {
                    persVotingRes.setColorFilter(itemView.getContext().getResources().getColor(R.color.trLightGreen));
                    break;
                }
                case "Egal": {
                    persVotingRes.setColorFilter(itemView.getContext().getResources().getColor(R.color.trLightOrange));
                    break;
                }
                default: {
                    persVotingRes.setColorFilter(itemView.getContext().getResources().getColor(R.color.trLightRed));
                    break;
                }
            }
        }
    }
}
