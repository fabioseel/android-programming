package com.studios.bobbycarpirat.wgapplication.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.studios.bobbycarpirat.wgapplication.R;
import com.studios.bobbycarpirat.wgapplication.ui.fragments.GroupChoosingFragment;
import com.studios.bobbycarpirat.wgapplication.ui.fragments.TodoFragment;
import com.studios.bobbycarpirat.wgapplication.ui.fragments.VotingDetailFragment;
import com.studios.bobbycarpirat.wgapplication.ui.fragments.VotingFragment;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.IdManager;
import com.studios.bobbycarpirat.wgapplication.ui.helperClasses.SharedPreferencesKeys;

public class MainActivity extends AppCompatActivity {

    static DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    static DatabaseReference mTestRef = mRootRef.child("Benutzer");

    private DrawerLayout mDrawerLayout;
    private final VotingFragment votingFragment = new VotingFragment();
    private final TodoFragment todoFragment = new TodoFragment();
    private final GroupChoosingFragment groupChoosingFragment = new GroupChoosingFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.burger_menu);
        loadFragment(votingFragment);

        // Setup the side navigation
        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        //set username from preferences
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPref.contains(SharedPreferencesKeys.USERNAME)){
            TextView greetingTextview = headerLayout.findViewById(R.id.textViewUserName);
            greetingTextview.setText(sharedPref.getString(SharedPreferencesKeys.USERNAME,"Adrian"));
        }
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        switch (menuItem.getItemId()) {
                            case R.id.nav_poll:
                                loadFragment(votingFragment);
                                break;
                            case R.id.nav_todo:
                                loadFragment(todoFragment);
                                break;
                            case R.id.nav_wg:
                                loadFragment(groupChoosingFragment);
                                break;
                            case R.id.nav_login:
                                Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                                startActivity(loginIntent);
                                break;
                            default:
                                break;
                        }
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();
                        return true;
                    }
                });


        SharedPreferences preferences = getSharedPreferences(SharedPreferencesKeys.USER_INFO, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(SharedPreferencesKeys.USER_ID, "b0001");
        editor.commit();

        /*String user_id = preferences.getString(SharedPreferencesKeys.USER_ID, "Leer");
        if (user_id == "Leer") {
            String new_user_id = IdManager.getUniqueId();
            editor.putString(SharedPreferencesKeys.USER_ID, new_user_id);
            editor.commit();
            mTestRef.child(new_user_id).child("Benutzername").setValue("New User");
        }*/

        IdManager.init();
    }

    @Override
    public void onResume(){
        super.onResume();
        // Set Title and Menu Selection to Voting (Standard)
        updateTitleAndMenu(VotingFragment.class.getName());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // Skip back to last item in Fragment manager, since its empty (first transition)
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
            updateTitleAndMenu(getSupportFragmentManager().getBackStackEntryAt(0).getName());
        }
    }

    public void showVoting(String votingName, String votingDesc, String votingId) {
        Fragment detailFragment = new VotingDetailFragment();
        Bundle arguments = new Bundle();
        arguments.putString(VotingDetailFragment.EXTRA_VOTING_NAME, votingName);
        arguments.putString(VotingDetailFragment.EXTRA_VOTING_DESC, votingDesc);
        arguments.putString(VotingDetailFragment.EXTRA_VOTING_ID,   votingId);
        detailFragment.setArguments(arguments);
        loadFragment(detailFragment);
        setTitle(votingName);
    }

    public void showVotingFragment() {
        Fragment votingFragment = new VotingFragment();
        loadFragment(votingFragment);
    }

    private void loadFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);


        if (!fragmentPopped && manager.findFragmentByTag(backStateName) == null) { // Fragment not in back stack, create it.
            if(fragment.getClass()==TodoFragment.class                           // If the Fragment is one of the "Level 1 Fragments", who can be accesed via the main menu, the back stack needs to be emptied
                    || fragment.getClass() == GroupChoosingFragment.class
                    || fragment.getClass() == VotingFragment.class)
            {
                while (manager.getBackStackEntryCount() > 0)
                    manager.popBackStackImmediate();
            }
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame, fragment, backStateName);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
        updateTitleAndMenu(fragment.getClass().getName());
    }

    private void updateTitleAndMenu(String fragmentClassName) {
        // Nicht die feinste Methode, aber für die Zwecke ausreichend:
        // Abgleich des Titles und korrekte Auswahl des Items im Drawer Menu passend zum Fragment
        NavigationView navigationView = findViewById(R.id.nav_view);
        if (fragmentClassName == (VotingFragment.class.getName())) {
            setTitle(R.string.title_abstimmungen);
            navigationView.setCheckedItem(R.id.nav_poll);
        } else if (fragmentClassName == (TodoFragment.class.getName())) {
            setTitle(R.string.title_todos);
            navigationView.setCheckedItem(R.id.nav_todo);
        } else if (fragmentClassName == (GroupChoosingFragment.class.getName())) {
            setTitle(R.string.title_wg_selection);
            navigationView.setCheckedItem(R.id.nav_wg);
        }
    }
}
