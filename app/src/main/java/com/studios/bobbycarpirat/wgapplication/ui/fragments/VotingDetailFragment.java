package com.studios.bobbycarpirat.wgapplication.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.studios.bobbycarpirat.wgapplication.R;
import com.studios.bobbycarpirat.wgapplication.ui.adapter.PersVotingRecyclerAdapter;

import java.util.HashMap;

public class    VotingDetailFragment extends Fragment {
    public static final String EXTRA_VOTING_NAME = "voting_name";
    public static final String EXTRA_VOTING_DESC = "voting_desc";
    public static final String EXTRA_VOTING_ID   = "voting_id";

    private TextView votingShortName;
    TextView extendedVotingDescription;
    TextView votingGreen;
    TextView votingOrange;
    TextView votingRed;
    RecyclerView persVotingRecyclerView;

    final DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
    final DatabaseReference mTestRef = mRootRef.child("Stimme");
    final DatabaseReference nTestRef = mRootRef.child("Benutzer");

    boolean initList;
    //  Abstimmungsergebnisse: {Anzahl Ja, Anzahl Egal, Anzahl Nein}
    final int[] results = {0, 0, 0};
    HashMap<String, String> userID_to_user;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_voting_detail, container, false);
        votingShortName = view.findViewById(R.id.voting_short);
        extendedVotingDescription =view.findViewById(R.id.ext_vot_desc);
        votingGreen = view.findViewById(R.id.vot_overview_green);
        votingOrange = view.findViewById(R.id.vot_overview_orange);
        votingRed = view.findViewById(R.id.vot_overview_red);
        persVotingRecyclerView = view.findViewById(R.id.pers_voting_recycler_view);
        String votingName = getArguments().getString(EXTRA_VOTING_NAME);
        votingShortName.setText(votingName);
        String votingDesc = getArguments().getString(EXTRA_VOTING_DESC);
        extendedVotingDescription.setText(votingDesc);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getContext());
        persVotingRecyclerView.setLayoutManager(mLayoutManager);
        persVotingRecyclerView.setItemAnimator(new DefaultItemAnimator());
        persVotingRecyclerView.addItemDecoration(new DividerItemDecoration(this.getContext(), LinearLayoutManager.VERTICAL));

        ValueEventListener userEntryListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                userID_to_user = new HashMap<>();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    HashMap<String, String> test = (HashMap<String, String>) snapshot.getValue();
                    userID_to_user.put(snapshot.getKey(), test.get("Benutzername"));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        nTestRef.addValueEventListener(userEntryListener);

        initList = true;
        ValueEventListener testEntryListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (initList) {
                    initList(dataSnapshot);
                    setVotingRes(results[0], results[1], results[2]);
                    initList = false;
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        };
        mTestRef.addValueEventListener(testEntryListener);

        return view;
    }

    private void setVotingRes(int green, int orange, int red) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) votingGreen.getLayoutParams();
        params.weight = green;
        votingGreen.setLayoutParams(params);
        if (green > 0) {
            votingGreen.setText("" + green);
        }
        else votingGreen.setText("");

        params = (LinearLayout.LayoutParams) votingOrange.getLayoutParams();
        params.weight = orange;
        votingOrange.setLayoutParams(params);
        if (orange > 0) {
            votingOrange.setText("" + orange);
        }
        else votingOrange.setText("");

        params = (LinearLayout.LayoutParams) votingRed.getLayoutParams();
        params.weight = red;
        votingRed.setLayoutParams(params);
        if (red > 0) {
            votingRed.setText("" + red);
        }
        else votingRed.setText("");
    }

    private void initList(DataSnapshot dataSnapshot) {

        final HashMap<String, String> persVotingList = new HashMap<>();

        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            HashMap<String, String> test = (HashMap<String, String>) snapshot.getValue();
            if (getArguments().getString(EXTRA_VOTING_ID).equals(test.get("Umfrage_ID"))) {
                String result = test.get("Antwort");
                switch (result) {
                    case ("Ja"):
                        results[0]++;
                        break;
                    case ("Egal"):
                        results[1]++;
                        break;
                    case ("Nein"):
                        results[2]++;
                        break;
                }
                persVotingList.put(getUsernameFromId(test.get("Benutzer_ID")), result);
            }
        }

        PersVotingRecyclerAdapter votingAdapter = new PersVotingRecyclerAdapter(persVotingList);

        persVotingRecyclerView.setAdapter(votingAdapter);
    }

    private String getUsernameFromId(String userId) {
        return userID_to_user.get(userId);
    }
}
