package com.studios.bobbycarpirat.wgapplication;

import com.studios.bobbycarpirat.wgapplication.model.Todo;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class newTodoUnitTest {

    @Test
    public void newTodoCreated() {
        Todo testTodo = new Todo("Giraffe",false,9876);
        assertNotNull(testTodo);
    }

    @Test
    public void todoHasRightDoneString(){
        Todo testTodo = new Todo("Giraffe",false,9876);
        assertEquals("9876",testTodo.getCustomidString());
    }
}